package com.epam.response;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class ClientResponse {

    private final String STATUS_CODE_MESSAGE = "Server status code: %s";
    private Response response;

    public ClientResponse(Response response) {
        this.response = response;
    }

    public int getStatusCode() {
        System.out.println(String.format(STATUS_CODE_MESSAGE, response.getStatusCode()));
        return response.getStatusCode();
    }

    public String getJsonValue(String key) {
        String value = new JsonPath(response.body().asInputStream()).getString(key);
        if (value == null) return "null";
        return value;
    }

    public <T> T getResponseAs(Class<T> aClass) {
        return response.as(aClass);
    }


}
