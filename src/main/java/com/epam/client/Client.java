package com.epam.client;

import com.epam.utils.FieldExclusion;
import com.google.gson.GsonBuilder;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import static io.restassured.RestAssured.given;


public class Client {
    private final ContentType DEFAULT_CONTENT_TYPE = ContentType.JSON;
    private final String URI = "http://localhost:8080";

    public Client() {
        RestAssured.baseURI = URI;
    }

    private Object serialize(Object body) {
        Object payload = body;
        if (!(body instanceof String)) payload = new GsonBuilder().addSerializationExclusionStrategy(
                new FieldExclusion()
        ).create().toJson(body, body.getClass());
        return payload;
    }

    public Response get(String url) {
        return given().contentType(DEFAULT_CONTENT_TYPE)
                .log().all()
                .when()
                .get(url)
                .then().log()
                .all()
                .extract().response();

    }

    public Response post(String url, Object body) {
        return given().contentType(DEFAULT_CONTENT_TYPE)
                .body(serialize(body)).log().all()
                .when()
                .post(url).then().log().all()
                .extract().response();

    }

    public Response put(String url, Object boby) {
        return given().contentType(DEFAULT_CONTENT_TYPE)
                .body(serialize(boby)).log().all()
                .when()
                .put(url).then().extract().response();

    }

    public Response delete(String url) {
        return given().contentType(DEFAULT_CONTENT_TYPE).when()
                .delete(url).then().extract().response();

    }
}
