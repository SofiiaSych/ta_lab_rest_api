package com.epam.models.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AuthorResponse{

	@JsonProperty("nationality")
	private String nationality;

	@JsonProperty("authorName")
	private AuthorName authorName;

	@JsonProperty("birth")
	private Birth birth;

	@JsonProperty("authorDescription")
	private String authorDescription;

	@JsonProperty("authorId")
	private int authorId;

	public void setNationality(String nationality){
		this.nationality = nationality;
	}

	public String getNationality(){
		return nationality;
	}

	public void setAuthorName(AuthorName authorName){
		this.authorName = authorName;
	}

	public AuthorName getAuthorName(){
		return authorName;
	}

	public void setBirth(Birth birth){
		this.birth = birth;
	}

	public Birth getBirth(){
		return birth;
	}

	public void setAuthorDescription(String authorDescription){
		this.authorDescription = authorDescription;
	}

	public String getAuthorDescription(){
		return authorDescription;
	}

	public void setAuthorId(int authorId){
		this.authorId = authorId;
	}

	public int getAuthorId(){
		return authorId;
	}

	@Override
 	public String toString(){
		return 
			"AuthorResponse{" + 
			"nationality = '" + nationality + '\'' + 
			",authorName = '" + authorName + '\'' + 
			",birth = '" + birth + '\'' + 
			",authorDescription = '" + authorDescription + '\'' + 
			",authorId = '" + authorId + '\'' + 
			"}";
		}
}