package com.epam.models.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AuthorName{

	@JsonProperty("first")
	private String first;

	@JsonProperty("second")
	private String second;

	public void setFirst(String first){
		this.first = first;
	}

	public String getFirst(){
		return first;
	}

	public void setSecond(String second){
		this.second = second;
	}

	public String getSecond(){
		return second;
	}

	@Override
 	public String toString(){
		return 
			"AuthorName{" + 
			"first = '" + first + '\'' + 
			",second = '" + second + '\'' + 
			"}";
		}
}