package com.epam.models.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Birth{

	@JsonProperty("date")
	private String date;

	@JsonProperty("country")
	private String country;

	@JsonProperty("city")
	private String city;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setCountry(String country){
		this.country = country;
	}

	public String getCountry(){
		return country;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	@Override
 	public String toString(){
		return 
			"Birth{" + 
			"date = '" + date + '\'' + 
			",country = '" + country + '\'' + 
			",city = '" + city + '\'' + 
			"}";
		}
}