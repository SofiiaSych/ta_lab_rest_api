package com.epam.models;

public class Author {
    private int authorId;
    private Name authorName;
    private String nationality;
    private Birth birth;
    private String authorDescription;

    public static class Name {
        private String first;
        private String second;

        public Name(String first, String second) {
            this.first = first;
            this.second = second;
        }

        @Override
        public String toString() {
            return "Name{" +
                    "firstName='" + first + '\'' +
                    ", secondName='" + second + '\'' +
                    '}';
        }
    }

    public static class Birth {
        private String date;
        private String country;
        private String city;

        public Birth() {
        }

        public Birth(String date, String country, String city) {
            this.date = date;
            this.country = country;
            this.city = city;
        }

        @Override
        public String toString() {
            return "Birth{" +
                    "date=" + date +
                    ", country='" + country + '\'' +
                    ", city='" + city + '\'' +
                    '}';
        }
    }

    public Author(int authorId, Name authorName, String nationality, Birth birth, String authorDescription) {
        this.authorId = authorId;
        this.authorName = authorName;
        this.nationality = nationality;
        this.birth = birth;
        this.authorDescription = authorDescription;
    }

    @Override
    public String toString() {
        return "Author{" +
                "authorId=" + authorId +
                ", authorName=" + authorName +
                ", nationality='" + nationality + '\'' +
                ", birth=" + birth +
                ", authorDescription='" + authorDescription + '\'' +
                '}';
    }
}
