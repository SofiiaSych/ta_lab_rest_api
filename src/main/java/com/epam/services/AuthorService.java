package com.epam.services;

import com.epam.client.Client;
import com.epam.models.Author;
import com.epam.response.ClientResponse;

public class AuthorService {

    private Client client;
    private Author author;

    public AuthorService() {
        client = new Client();
    }

    public ClientResponse createAuthor(int authorId) {
        author = new Author(authorId, new Author.Name("Arturo", "Roman"), "Spanish",
                new Author.Birth("1973-03-28",
                        "Spain","Madrid"), "Some description");

        return new ClientResponse(client.post("/api/library/author", author));
    }

    public ClientResponse getAuthors() {
        return new ClientResponse(client.get("/api/library/authors"));
    }

    public ClientResponse getAuthorById(int authorId) {
        return new ClientResponse(client.get("/api/library/author/" + authorId));
    }

    public ClientResponse getAuthorsByFirstName(String authorFirstName) {
        return new ClientResponse(client.get("/api/library/authors/search?query=" + authorFirstName));
    }

    public ClientResponse getAuthorBySpecialBook(int bookId) {
        return new ClientResponse(client.get("/api/library/book/" + bookId + "/author"));
    }

    public ClientResponse updateAuthor(int authorId) {
        author = new Author(authorId, new Author.Name("Arturo", "Roman"), "Spanish",
                new Author.Birth("1973-03-28",
                        "Spain","Barcelona"), "None descriptions");
        return new ClientResponse(client.put("/api/library/author", author));
    }

    public ClientResponse deleteAuthorById(int authorId, boolean isForcibly) {
        return new ClientResponse(client.delete("/api/library/author/" + authorId + "?forcibly=" + isForcibly));
    }
}

