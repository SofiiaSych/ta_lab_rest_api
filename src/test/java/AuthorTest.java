import com.epam.models.response.AuthorResponse;
import com.epam.response.ClientResponse;
import com.epam.services.AuthorService;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.ThreadLocalRandom;

import static java.util.Arrays.stream;

public class AuthorTest {

    private AuthorService authorService;
    private ClientResponse clientResponse;
    private Integer authorId;


    @BeforeClass
    public void serviceSetup() {
        authorService = new AuthorService();
        authorId = ThreadLocalRandom.current().nextInt(2, 2100 + 1);
    }

    @Test(priority = 1)
    public void postAuthorTest() {
        clientResponse = authorService.createAuthor(authorId);
        Assert.assertEquals(clientResponse.getStatusCode(), 201, "Wrong status code");
        Assert.assertEquals(clientResponse.getJsonValue("authorName.first"), "Arturo", "Author name is different as expected");
        Assert.assertEquals(clientResponse.getJsonValue("authorName.second"), "Roman", "Author surname is different as expected");
        Assert.assertEquals(clientResponse.getJsonValue("birth.date"), "1973-03-28", "Author birth date is different as expected");
    }

    @Test(priority = 2)
    public void getAuthorsTest() {
        clientResponse = authorService.getAuthors();
        Assert.assertEquals(clientResponse.getStatusCode(), 200, "Wrong status code");
        AuthorResponse response = stream(clientResponse.getResponseAs(AuthorResponse[].class))
                .filter(x -> x.getAuthorId() == authorId).findFirst().get();
        Assert.assertEquals(response.getAuthorName().getFirst(), "Arturo", "Author name is different as expected");
        Assert.assertEquals(response.getAuthorName().getSecond(), "Roman", "Author surname is different as expected");
        Assert.assertEquals(response.getBirth().getDate(), "1973-03-28", "Author birth date is different as expected");

    }

    @Test(priority = 3)
    public void getAuthorByIdTest() {
        clientResponse = authorService.getAuthorById(398);
        Assert.assertEquals(clientResponse.getStatusCode(), 200, "Wrong status code");
        Assert.assertEquals(clientResponse.getJsonValue("authorName.first"), "Irma", "Author name is different as expected");
        Assert.assertEquals(clientResponse.getJsonValue("authorName.second"), "Marquardt", "Author surname is different as expected");
        Assert.assertEquals(clientResponse.getJsonValue("birth.country"), "Gambia", "Author birth country is different as expected");
    }

    @Test(priority = 4)
    public void getAuthorByFirstNameTest() {
        clientResponse = authorService.getAuthorsByFirstName("Irma");
        Assert.assertEquals(clientResponse.getStatusCode(), 200, "Wrong status code");
        Assert.assertEquals(clientResponse.getJsonValue("authorName.first"), "[Irma]", "Author name is different as expected");
    }

    @Test(priority = 5)
    public void getAuthorBySpecialBookTest() {
        clientResponse = authorService.getAuthorBySpecialBook(82);
        Assert.assertEquals(clientResponse.getStatusCode(), 200, "Wrong status code");
        Assert.assertEquals(clientResponse.getJsonValue("authorName.first"), "Roscoe", "Author name is different as expected");
        Assert.assertEquals(clientResponse.getJsonValue("authorName.second"), "Hills", "Author surname is different as expected");
        Assert.assertEquals(clientResponse.getJsonValue("nationality"), "Cuban", "Author nationality is different as expected");
    }

    @Test(priority = 6)
    public void getNonExistentAuthorTest() {
        clientResponse = authorService.getAuthorById(435);
        Assert.assertEquals(clientResponse.getStatusCode(), 404, "This author has already existed");
        Assert.assertEquals(clientResponse.getJsonValue("errorMessage"), "Author with 'authorId' = '435' doesn't exist!");

    }

    @Test(priority = 7)
    public void putAuthorTest() {
        clientResponse = authorService.updateAuthor(authorId);
        Assert.assertEquals(clientResponse.getStatusCode(), 200, "Wrong status code");
        Assert.assertEquals(clientResponse.getJsonValue("authorName.first"), "Arturo", "Author name is different as expected");
//        Assert.assertEquals(clientResponse.getJsonValue("birth.city"), "Barcelona", "Author city is different as expected");
    }

    @Test(priority = 8)
    public void deleteAuthorTest() {
        clientResponse = authorService.deleteAuthorById(authorId, false);
        Assert.assertEquals(clientResponse.getStatusCode(), 204, "Wrong status code");
    }
}

